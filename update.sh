#!/bin/bash

PATH=$PATH:/root/.rbenv/plugins/ruby-build/bin:/root/.rbenv/shims:/root/.rbenv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export PATH

cd $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
git reset --hard HEAD
git pull
composer install
composer dumpautoload
chown -R www-data:www-data .
