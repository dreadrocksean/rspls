<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Round;

class GameController extends Controller
{
    private function getChoiceName($index)
    {
        $names = array('rock','paper','scissors','spock','lizard');
        return  $names[$index];
    }

    private function updateStats($computer, $player)
    {
        $winner = null;
        if ($player != $computer) {
            $winner = $this->getWinner($computer, $player);
        }
        return $this->createRound($computer, $player, $winner);
    }

    private function getWinner($computer, $player)
    {
        $flip = ($player - $computer) < 0? 1: 0;
        return ((abs($player - $computer) % 2) + $flip) % 2;
    }

    /**
     * @Route("/game")
     */
    public function gameAction()
    {
        $title = 'RPSSL';
        $winningStats = $this->getWinningStats();
        $choiceStats = $this->getChoiceStats();
        return $this->render('game/number.html.twig', array(
            'title' => $title,
            'choiceStats' => $this->getChoiceStats(),
            'winningStats' => $this->getWinningStats(),
        ));
    }
    /**
     * @Route("/api/choose/{player}")
     */
    public function chooseAction($player)
    {
        $computer = $this->getRandomPlay();
        $round = $this->updateStats($computer, (int)($player));
        $response = new JsonResponse();
        $response->setData(array(
            'status' => 200,
            'round' => array(
                'computer' => $round->getCChoice(),
                'player' => $round->getPChoice(),
                'winner' => $round->getWinner(),
            ),
            'winningStats' => $this->getWinningStats(),
            'choiceStats' => $this->getChoiceStats(),
        ));
        return $response;
    }
    /**
     * @Route("/api/stats")
     */
    public function statsAction()
    {
        $response = new JsonResponse();
        $response->setData(array(
            'status' => 200,
            'winningStats' => $this->getWinningStats(),
            'choiceStats' => $this->getChoiceStats(),
        ));
        return $response;
    }
    /**
     * @Route("/api/delete")
     */
    public function deleteAction()
    {
        try {
            $this->deleteStats();
        } catch (\Exception $e) {
            $response = new JsonResponse();
            $response->setData(array(
                'error' => $e,
            ));
            return $response;
        }
        return $this->statsAction();
    }

    private function getRandomPlay()
    {
        return mt_rand(0, 4);
    }

    private function getRound($id)
    {
        $round = $this->getDoctrine()
            ->getRepository('AppBundle:Round')
            ->find($id);

        if (!$round) {
            throw $this->createNotFoundException(
                'No round found for id '.$id
            );
        }

        return $round;
    }

    private function createRound($computer, $player, $winner)
    {
        $round = new Round();
        $round->setCChoice($computer);
        $round->setPChoice($player);
        $round->setWinner($winner);

        $em = $this->getDoctrine()->getManager();
        $em->persist($round);
        $em->flush();

        return $round;
    }

    private function getWinningStats()
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $query = $em->createQuery(
                'SELECT r.winner, COUNT(1) AS total
                FROM AppBundle:Round r
                WHERE r.winner = :winner'
            )->setParameter('winner', 0);
            $cRounds = $query->getOneOrNullResult();

            $query = $em->createQuery(
                'SELECT COUNT(1) AS total
                FROM AppBundle:Round r
                WHERE r.winner = :winner'
            )->setParameter('winner', 1);
            $pRounds = $query->getOneOrNullResult();

            return [$cRounds, $pRounds];
        } catch (\Exception $e) {
            return $e;
        }

    }

    private function getChoiceStats()
    {
        try {
            $em = $this->getDoctrine()->getManager();

            $query = $em->createQuery(
                'SELECT r.c_choice, COUNT(r.c_choice) AS total
                FROM AppBundle:Round r
                GROUP BY r.c_choice'
            );
            $cTotals = $query->getResult();

            $query = $em->createQuery(
                'SELECT r.p_choice, COUNT(r.p_choice) AS total
                FROM AppBundle:Round r
                GROUP BY r.p_choice'
            );
            $pTotals = $query->getResult();

            return $this->formatChoiceStats([$cTotals, $pTotals]);
        } catch (\Exception $e) {
            return $e;
        }
    }

    private function formatChoiceStats($resultsArray) {
        for ($j = 0; $j < 2; $j++) {
            $tempArray = [];
            $key = $j == 0? 'c_choice': 'p_choice';
            for ($i = 0; $i < 5; $i++) {
                if (!array_key_exists($i, $resultsArray[$j])) {
                    $resultsArray[$j][$i] = array($key => $i, 'total' => 0);
                } elseif ($i != (int)($resultsArray[$j][$i][$key])) {
                    $index = $resultsArray[$j][$i][$key];
                    $total = $resultsArray[$j][$i]['total'];

                    if (array_key_exists($index, $resultsArray[$j])) {
                        $tempArray[] = $resultsArray[$j][$index];
                    }
                    $resultsArray[$j][$index] = array($key => $index, 'total' => $total);
                    $resultsArray[$j][$i] = array($key => $i, 'total' => 0);
                }
            }
            foreach ($tempArray as $i => $val) {
                $index = $val[$key];
                $total = $val['total'];
                $resultsArray[$j][$index] = array($key => $index, 'total' => $total);
            }
        }
        return $resultsArray;
    }

    private function deleteStats()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'DELETE FROM AppBundle:Round r'
        );
        return $query->getResult();
    }
}
