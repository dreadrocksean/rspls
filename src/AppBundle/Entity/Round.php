<?php
// src/AppBundle/Entity/Round.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Rounds")
 */

class Round
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $c_choice;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $p_choice;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $winner;

    public function setCChoice($choice)
    {
    	$this->c_choice = $choice;
    }

    public function setPChoice($choice)
    {
    	$this->p_choice = $choice;
    }

    public function setWinner($winner)
    {
    	$this->winner = $winner;
    }

    public function getCChoice()
    {
    	return $this->c_choice;
    }

    public function getPChoice()
    {
    	return $this->p_choice;
    }

    public function getWinner()
    {
    	return $this->winner;
    }
}
