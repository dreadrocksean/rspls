window.RPSLS = window.RPSLS || {};
RPSLS.init = (function(init) {
	RPSLS.options = [
		'Rock',
		'Paper',
		'Scissors',
		'Spock',
		'Lizard',
	];

	RPSLS.showStats = function(r) {
		var cWins = r.winningStats[0].total;
		var pWins = r.winningStats[1].total;
		var cStats = r.choiceStats[0];
		var pStats = r.choiceStats[1];

		$('.c_wins').html(cWins);
		$('.p_wins').html(pWins);
		
		$('.c-rock').html(cStats[0].total);
		$('.c-paper').html(cStats[1].total);
		$('.c-scissors').html(cStats[2].total);
		$('.c-lizard').html(cStats[4].total);
		$('.c-spock').html(cStats[3].total);

		$('.p-rock').html(pStats[0].total);
		$('.p-paper').html(pStats[1].total);
		$('.p-scissors').html(pStats[2].total);
		$('.p-lizard').html(pStats[4].total);
		$('.p-spock').html(pStats[3].total);
	};

	RPSLS.chooseOption = function(choice) {
		$('button.option').removeClass('chosen');
		$('.'+RPSLS.options[choice].toLowerCase()).addClass('chosen');
		$.get('/api/choose/'+choice, function(r) {
			$('button.option').removeClass('chosen');
			$('.'+RPSLS.options[r.round.player].toLowerCase()).addClass('chosen');

			$('.computer')
				.html(RPSLS.options[r.round.computer])
				.removeClass('winner hide')
				.addClass(r.round.winner === 0? 'winner': '');
			$('.player')
				.html(RPSLS.options[r.round.player])
				.removeClass('winner hide')
				.addClass(r.round.winner === 1? 'winner': '');

			RPSLS.showStats(r);
		});
	};

	RPSLS.clearStats = function() {
		$.get('/api/delete', function(r) {
			RPSLS.showStats(r);
			$('.choice').addClass('hide');
			$('button.option').removeClass('chosen');
		});
	};

	return init;
})(RPSLS.init || {});